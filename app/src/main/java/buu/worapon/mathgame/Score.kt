package buu.worapon.mathgame

class Score(var correct: Int =0, var incorrect: Int =0) {
    fun plusCorrect(){
        correct++
    }
    fun plusIncorrect(){
        incorrect++
    }
}

