package buu.worapon.mathgame.database

import androidx.lifecycle.LiveData
import androidx.room.*


@Dao
interface PlayerDatabaseDao {

    @Insert
    suspend fun insert(player: PlayerTable)

    @Update
    suspend fun update(player: PlayerTable)

    @Query("SELECT * from player_table where username = :name")
    suspend fun get(name: String): PlayerTable?

    @Query("SELECT * from player_table ORDER BY score_correct DESC")
    fun getScoreCorrect(): LiveData<List<PlayerTable>>

}