package buu.worapon.mathgame

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import buu.worapon.mathgame.database.PlayerDatabase
import buu.worapon.mathgame.databinding.FragmentCalculateBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


/**
 * A simple [Fragment] subclass.
 * Use the [CalculateFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class CalculateFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var menu = 0

    lateinit var binding: FragmentCalculateBinding
    private lateinit var calculateViewModel: CalculateViewModel
    private lateinit var viewModelFactory: CalculateViewModelFactory


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_calculate, container, false)
        val application = requireNotNull(this.activity).application
        val dataSource = PlayerDatabase.getInstance(application).playerDatabaseDao


        viewModelFactory = CalculateViewModelFactory(
            Score(
                CalculateFragmentArgs.fromBundle(requireArguments()).pointCorrect,
                CalculateFragmentArgs.fromBundle(requireArguments()).pointIncorrect
            ), CalculateFragmentArgs.fromBundle(requireArguments()).menu,
            dataSource, application
        )

        calculateViewModel =
            ViewModelProvider(this, viewModelFactory).get(CalculateViewModel::class.java)
        binding.calculateViewModel = calculateViewModel
        binding.setLifecycleOwner(this)


        requireActivity().onBackPressedDispatcher.addCallback(this) {
            view?.findNavController()?.navigate(
                CalculateFragmentDirections.actionCalculateFragmentToMainFragment(
                    calculateViewModel.score.value!!.correct,
                    calculateViewModel.score.value!!.incorrect
                )
            )
        }

        calculateViewModel.hasClicked.observe(viewLifecycleOwner, Observer { hasClicked ->
            binding.invalidateAll()
            if (hasClicked) {
                onCheckAnswer()
                calculateViewModel.onHasClickedFinished()
            }

        })

        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root

    }


    private fun onCheckAnswer() {
        calculateViewModel.checkResult()
        if (calculateViewModel.resultBoolean.value!!) {
            binding.txtAnswer.setTextColor(Color.parseColor("#00ff00"))
        } else {
            binding.txtAnswer.setTextColor(Color.parseColor("#ff0000"))
        }
    }

}