package buu.worapon.mathgame

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import buu.worapon.mathgame.database.PlayerDatabaseDao

class CalculateViewModelFactory(
    private val score: Score,
    private val menu1: Int,
    private val dataSource: PlayerDatabaseDao,
    private val application: Application): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CalculateViewModel::class.java)) {
            return CalculateViewModel(score,menu1,dataSource,application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}