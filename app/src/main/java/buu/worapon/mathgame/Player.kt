package buu.worapon.mathgame

class Player(
    playerId: Long = 0,
    username: String = "",
    scoreCorrect: Int = 0,
    scoreIncorrect: Int = 0
) {
    private var playerId: Long = playerId
    private var username: String = username
    private var scoreCorrect: Int = scoreCorrect
    private var scoreIncorrect: Int = scoreIncorrect


    fun setPlayerId (id: Long) {
        this.playerId = id
    }

    fun getPlayerId () = this.playerId

    fun setUsername (username: String) {
        this.username = username
    }

    fun getUsername () = this.username

    fun setScoreCorrect(scoreCorrect: Int) {
        this.scoreCorrect =scoreCorrect
    }

    fun getScoreCorrect() = this.scoreCorrect

    fun setScoreIncorrect(scoreIncorrect: Int) {
        this.scoreIncorrect = scoreIncorrect
    }

    fun plusCorrect(){
        this.scoreCorrect++
    }
    fun plusIncorrect(){
        this.scoreIncorrect++
    }
}