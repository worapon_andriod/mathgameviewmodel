package buu.worapon.mathgame

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import buu.worapon.mathgame.databinding.FragmentMainBinding
import kotlinx.android.synthetic.main.activity_main.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


/**
 * A simple [Fragment] subclass.
 * Use the [MainFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MainFragment : Fragment() {
    // TODO: Rename and change types of parameters

    private  var menu =0

    private lateinit var mainViewModel: MainViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentMainBinding>(inflater, R.layout.fragment_main,container,false)

        mainViewModel = MainViewModel(Score(MainFragmentArgs.fromBundle(requireArguments()).pointCorrect,MainFragmentArgs.fromBundle(requireArguments()).pointIncorrect))

        mainViewModel.eventClickBtnSum.observe(viewLifecycleOwner, Observer { eventClickBtnSum ->
            if(eventClickBtnSum){
                menu = 1
                view?.findNavController()?.navigate(MainFragmentDirections.actionMainFragmentToCalculateFragment(mainViewModel.score.value!!.correct,mainViewModel.score.value!!.incorrect,menu))
                mainViewModel.onClickBtnSumFinished()
            }
        })
        mainViewModel.eventClickBtnMinus.observe(viewLifecycleOwner, Observer { eventClickBtnMinus ->
            if(eventClickBtnMinus){
                menu = 2
                view?.findNavController()?.navigate(MainFragmentDirections.actionMainFragmentToCalculateFragment(mainViewModel.score.value!!.correct,mainViewModel.score.value!!.incorrect,menu))
                mainViewModel.onClickBtnMinusFinished()
            }
        })
        mainViewModel.eventClickBtnMulti.observe(viewLifecycleOwner, Observer { eventClickBtnMulti ->
            if(eventClickBtnMulti){
                menu = 3
                view?.findNavController()?.navigate(MainFragmentDirections.actionMainFragmentToCalculateFragment(mainViewModel.score.value!!.correct,mainViewModel.score.value!!.incorrect,menu))
                mainViewModel.onClickBtnMultiFinished()
            }

        })
        mainViewModel.eventClickBtnDivide.observe(viewLifecycleOwner, Observer { eventClickBtnDivide ->
            if(eventClickBtnDivide){
                menu = 4
                view?.findNavController()?.navigate(MainFragmentDirections.actionMainFragmentToCalculateFragment(mainViewModel.score.value!!.correct,mainViewModel.score.value!!.incorrect,menu))
                mainViewModel.onClickBtnDivideFinished()
            }
        })

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            requireActivity().finish()
        }

        binding.mainViewModel = mainViewModel
        return binding.root
    }


}