package buu.worapon.mathgame

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import buu.worapon.mathgame.database.PlayerTable
import buu.worapon.mathgame.database.PlayerDatabaseDao
import kotlin.random.Random

class CalculateViewModel(score: Score, menu: Int, val database: PlayerDatabaseDao,application: Application) : ViewModel() {

    private val _score = MutableLiveData<Score>()
    val score: LiveData<Score>
        get() = _score

    private val _num1 = MutableLiveData<Int>()
    val num1: LiveData<Int>
        get() = _num1

    private val _num2 = MutableLiveData<Int>()
    val num2: LiveData<Int>
        get() = _num2

    private val _txtSign = MutableLiveData<String>()
    val txtSign: LiveData<String>
        get() = _txtSign

    private val _btn1 = MutableLiveData<Int>()
    val btn1: LiveData<Int>
        get() = _btn1

    private val _btn2 = MutableLiveData<Int>()
    val btn2: LiveData<Int>
        get() = _btn2

    private val _btn3 = MutableLiveData<Int>()
    val btn3: LiveData<Int>
        get() = _btn3

    private val _txtAns = MutableLiveData<String>()
    val txtAns: LiveData<String>
        get() = _txtAns

    private val _result = MutableLiveData<Int>()
    val result: LiveData<Int>
        get() = _result

    private val _eventScoreChange = MutableLiveData<Boolean>()
    val eventScoreChange : LiveData<Boolean>
        get() = _eventScoreChange

    private val _menu = MutableLiveData<Int>()
    val menu : LiveData<Int>
        get() = _menu

    private val _resultBoolean = MutableLiveData<Boolean>()
    val resultBoolean : LiveData<Boolean>
        get() = _resultBoolean

    private val _hasClicked = MutableLiveData<Boolean>()
    val hasClicked : LiveData<Boolean>
        get() = _hasClicked

    private val _answer = MutableLiveData<Int>()
    val answer : LiveData<Int>
        get() = _answer

    private var player = MutableLiveData<PlayerTable?>()

    init {
       // initializePlayer()
        _resultBoolean.value = false
        _menu.value = menu
        _score.value = score
        _num1.value = 0
        _num2.value = 0
        _txtSign.value = ""
        _result.value = 0
        _txtAns.value = ""
        play(_menu.value!!)
    }

//    private fun initializePlayer() {
//        viewModelScope.launch {
//            player.value = getPlayerFromDatabase()
//        }
//    }

//    private suspend fun  getPlayerFromDatabase(): Player? {
//        return  database.get(id)
//    }

    private fun play(menu: Int) {
        _result.value = setQuestion(menu)
        randomButton(_result.value!!)

    }


     fun checkResult() {
        if(answer.value == result.value){
            ansCorrect()
        }else{
            ansIncorrect()
        }
         _eventScoreChange.value = true
         play(_menu.value!!)
    }


    private fun ansIncorrect() {
        _txtAns.value = "ผิด"
        _score.value?.plusIncorrect()
        _resultBoolean.value = false
    }

    private fun ansCorrect() {
       _txtAns.value = "ถูก"
        _score.value?.plusCorrect()
        _resultBoolean.value = true
    }

    private fun randomButton(result: Int) {
        val randomNum = Random.nextInt(1, 4)
        if (randomNum == 1) {
            _btn1.value = result
            _btn2.value = result?.plus(1)
            _btn3.value = result?.minus(1)
        } else if (randomNum == 2) {
            _btn1.value = result?.plus(1)
            _btn2.value = result
            _btn3.value = result?.minus(1)
        } else {
            _btn1.value = result?.plus(1)
            _btn2.value = result?.minus(1)
            _btn3.value = result

       }
    }



    private fun setQuestion(menu: Int): Int {
        var result = 0
        var number1 = Random.nextInt(0, 10)
        var number2 = Random.nextInt(0, 10)

        _num1.value = number1
        _num2.value = number2

        if (menu == 1){
            result = number1 + number2
            _txtSign.value = "+"
        }else if(menu == 2){
            result = number1 - number2
            _txtSign.value = "-"
        }else if (menu == 3) {
            result = number1 * number2
            _txtSign.value = "*"
        }else {
            while (true){
                number2 = Random.nextInt(1,10)
                if (number1 % number2 ==0){
                    result = number1 / number2
                    _txtSign.value = "/"
                    _num2.value = number2
                    break
                }
            }
        }
        return result
    }

    fun onHasClicked(answer: Int) {
        _answer.value = answer
        _hasClicked.value = true
        Log.i("result", _hasClicked.value.toString())
    }

    fun onHasClickedFinished() {
        _hasClicked.value = false
    }


}