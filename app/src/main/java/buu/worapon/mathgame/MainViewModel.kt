package buu.worapon.mathgame

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainViewModel (score: Score): ViewModel() {
    private val _score = MutableLiveData<Score>()
    val score : LiveData<Score>
        get() = _score

    private val _eventClickBtnSum = MutableLiveData<Boolean>()
    val eventClickBtnSum : LiveData<Boolean>
        get() = _eventClickBtnSum


    fun onClickBtnSum() {
        _eventClickBtnSum.value = true
    }

    fun onClickBtnSumFinished() {
        _eventClickBtnSum.value = false
    }

    private val _eventClickBtnMinus = MutableLiveData<Boolean>()
    val eventClickBtnMinus : LiveData<Boolean>
        get() = _eventClickBtnMinus


    fun onClickBtnMinus() {
        _eventClickBtnMinus.value = true
    }

    fun onClickBtnMinusFinished() {
        _eventClickBtnMinus.value = false
    }

    private val _eventClickBtnMulti = MutableLiveData<Boolean>()
    val eventClickBtnMulti : LiveData<Boolean>
        get() = _eventClickBtnMulti


    fun onClickBtnMulti() {
        _eventClickBtnMulti.value = true
    }

    fun onClickBtnMultiFinished() {
        _eventClickBtnMulti.value = false
    }

    private val _eventClickBtnDivide = MutableLiveData<Boolean>()
    val eventClickBtnDivide : LiveData<Boolean>
        get() = _eventClickBtnDivide


    fun onClickBtnDivide() {
        _eventClickBtnDivide.value = true
    }

    fun onClickBtnDivideFinished() {
        _eventClickBtnDivide.value = false
    }


    init{
        _score.value = score
    }
}